;;; bridge.el --- Experimental package for displaying visible connections  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Charlie Gordon

;; Author: Charlie Gordon <prayadtangban@gmail.com>
;; Keywords: multimedia, hypermedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Show visible connection between links via a child frame

;;; Code:



(provide 'bridge)
;;; bridge.el ends here
